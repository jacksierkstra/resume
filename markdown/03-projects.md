# Projects
In this section there will be an overview of my work experience.

## Software Engineer @ Info Support B.V.
*Period: 2015 - Present*

Info Support is providing IT consultancy services to a broad range of companies. I am doing onsight consultancy which entails developing new business features and improving their codebase.

Several hightlights include
- Gave a course about microservices to hundred students.
- Visited Oracle Code One and did a redelivery about that to colleagues.
- Be the face of Info Support at a conference (JFall) stand.

## Full stack developer @ BeFrank
*Period: 2018 - Present*

BeFrank is a Pensioen Premie Instelling (PPI), which is basically a middle man between the insurance company and the person that will eventually receive their pension. BeFrank is giving a detailed insights into someones pension.

Several hightlights include
- Did a database migration of 18 microservices from PostgreSQL to SQL Server whilst critical business applications kept running.
- Moved the complete infrastructure and release management from Docker Swarm to Kubernetes.
- Contributed to an open source project: https://github.com/GoogleContainerTools/jib/issues/2585.
- Worked in several 3~5 people agile teams delivering business features enabling BeFrank to grow.
- Created new processes in Activitii and developing new microservices in Spring Boot.
                                   
## Android Developer @ ING Nederland
*Period: 2017 - 2018*

As mentioned in the previous ING project, the company is big and this resulted in an opportunity that had more customer impact. The Android banking app, which had 3.2 million active users per month, needed to include a mobile payments module (NFC).

Several hightlights include
- Created and launched an Android Mobile Payments module in the ING Banking app which had 3.2 million active users at the time.
- Started learning Android without prior knowledge and quickly became comfortable with it.
- Transferred our codebase from Java to Kotlin.

## Full stack developer @ ING Nederland
*Period: 2015 - 2017*

ING is the biggest bank in the Netherlands with somewhere in the regions of 8 million customers. Because of that, the company has quite a few of departments. I was part of the consumer lending. The stack consisted of a Spring based backend and an AngularJS application (customer facing).

Several hightlights include
- Did frontend and backend development (Full stack).
- Made Acceptance Test Driven Development (ATDD) a joint effort between business and IT.
- Introduced a technique (Baker) that made our processes a state machine, letting the business define steps.


## Full stack developer @ Translink B.V.
*Period: 2015 - 2015*

Translink is the company that created the OV-chipkaart, which is the Dutch public transport card. I had to make a webportal where companies could replace their business card.

Hightlight
- Made a Dockerized webportal from scratch in Spring Boot.
