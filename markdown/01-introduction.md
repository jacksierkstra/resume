## Introduction
If you want to work with somebody that has passion for (new) technologies, strives for clean and consistent code, is communicative and is very open and transparant? Then you should hire me because I check those boxes. With five+ years of experience in the field, I can definitely be an added value to yourcompany.

Besides that, I enjoy music, a good beer, sports (both watching and participating) and being with friends and family. I also have a weak spot for Linux because it is so customizable.