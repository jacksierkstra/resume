template=$(<index.template.html)

header=$(npm run --silent render ./markdown/00-header.md)
introduction=$(npm run --silent render ./markdown/01-introduction.md)
aboutMyPlans=$(npm run --silent render ./markdown/02-about-my-plans.md)
projects=$(npm run --silent render ./markdown/03-projects.md)
education=$(npm run --silent render ./markdown/04-education.md)

rm index.result.html 2> /dev/null

echo "${template//__HEADER__/$header}" > index.result.html
template=$(<index.result.html)
echo "${template//__INTRODUCTION__/$introduction}" > index.result.html
template=$(<index.result.html)
echo "${template//__ABOUT_MY_PLANS__/$aboutMyPlans}" > index.result.html
template=$(<index.result.html)
echo "${template//__PROJECTS__/$projects}" > index.result.html
template=$(<index.result.html)
echo "${template//__EDUCATION__/$education}" > index.result.html

echo "Replacing image references."
template=$(<index.result.html)
echo "${template//"./../img/"/"./img/"}" > index.result.html

echo "Done, enjoy the result in index.result.html."